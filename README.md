# x509 Client Authentication

	Copyright (c) 2016 thomas.zink_at_uni-konstanz_dot_de (tz)	
	
	Usage of the works is permitted provided that this instrument is retained with the works, so that any entity that uses the works is notified of this instrument.
	DISCLAIMER: THE WORKS ARE WITHOUT WARRANTY. 

## Description

Provides a PHP interface class `clientcert.php` for easy access of x509 Client Certificates.
The main purpose for this is to use client certificates as proof of identity for authentication and login for web-based applications.
This can also be used for easy certificate-based two-factor-authentication (2FA).

Also provided are vagrant and ansible files to auto-install a test VM.
An example `index.html` demonstrates the usage of the class.

### Two-Factor-Authentication (2FA)

For certificate-based 2FA, the application must ensure that the user's certificate and identity match and are dependent of each other.
In general, the user's certificate should act as the user's identity and username for login.
In this scenario, the user has to provide his certificate as user ID and then enter his password.
This satisfies the _what you have_ (certificate) and _what you know_ (password) principles of 2FA.

Most commonly, the username is equal to either the email address or the local-part of the email address.
Both can be easily extracted from the users certificate, which is supported by the class `clientcert.php`.
In these cases, the users client certificate can be used to provide the actual login name (username) to the application.
This enables the user to login with his certificate and his application password.

The certificate should be stored securely either encrypted and protected by a password, or better on a smartcard or USB cryptoken.
Note that the password protecting the certificate is _not_ enforced or checked by the server application, and as such is not part of the authentication process.
This is why password-protected certificates alone are not considered two authentication factors (also applies to sshkeys).
Also, if your application does not check the certificate's identity, you again do not achieve true 2FA, since a user can potentially log in with a valid certificate and an independent valid username.
Provided that the user uses his email address as username, this can be achieved by extracting the username from the users certificate and set the login username accordingly.

An example for correct usage of `clientcert.php` for true 2FA is provided in `playbooks/files/index.html`.

## Usage

First, check that your webserver supports SSL and client authentication.
Set webserver options accordingly and expose the client certificate data and standard variables.
Check your webservers documentation on how to do that.

Here is an example for **apache2** (also see `playbooks/templates/ssl.apache.j2`).

	SSLOptions +ExportCertData +StdEnvVars
	SSLVerifyClient require
	SSLVerifyDepth  10
	
To enable SSL support run

	a2enmod ssl

To use the class simply include it in your application/project and create a new instance.
For example:

	require_once('clientcert.php');
	$cert = new ClientCert();

Then set the username input field of your applications login screen to readonly and set the username to the user's email (or other login info extracted from the certificate).

For example:

	<input type='text' name='user' value='<?php echo $cert->getEmail(); ?>' readonly /> <br/>

This is done only to inform the user about his username. The input field **must not** be evaluated for authentication. The username is explicitly only set by a call to `$cert->getEmail()` or `$cert->getLocalpart()`, dependending on your specific setup. That must be done to ensure that the username is extracted from the certificate and not set or overridden by the client. Only the password input is provided by the user.

Congratulations. You just enabled true certificate-based 2FA authentication for your web application.

## Testing

To use the vagrant test machine you need to have vagrant and ansible installed, as well as have some VM provider, like Virtualbox.
To test the functionality you also need to have a client certificate installed in your browser.

_Beware: Using private browsing mode might interfere with client certificate authentication and lead to unexpected behavior._

To run the test virtual machine `cd playbooks` and simply execute

	vagrant up

If for some reason you get an `UNREACHABLE` error, something is probably wrong with the ssh keys.
If this happens, try running

	vagrant provision

More troubleshooting: use `vagrant ssh` and check `/home/vagrant/.ssh/authorized_keys`.
Compare that with the output of `vagrant ssh-config` and the argument `ansible_ssh_private_key_file` in `.vagrant/provisioners/ansible/inventory`.

Vagrant installs the VM and ansible installs all required packages like apache2 and php.
It also copies over the apache configuration as well as `clientcert.php` and `index.html`.

After ansible has finished open a browser and navigate to [https://localhost:8443](https://localhost:8443).
The browser will ask for your certificate and issue warnings, that the site is not secure.
This is expected, since the vm uses prepackaged `snakeoil` certificates for authentication.

If all goes well you will see some information about your certificate as well has be presented with a dummy login form that has your certificate's username prefilled.
It does not provide actual login functionality.

## Software Versions

Developed and tested with the following software versions.

Host
- Ubuntu 16.04.1 LTS
- Vagrant 1.8.5
- ansible 2.0.0.2

Guest
- Ubuntu 14.04.5 LTS
- Apache/2.4.7 (Ubuntu)
- PHP 5.5.9-1ubuntu4.20
