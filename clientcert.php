<?php

/*
	Class ClientCert
	Parses a x509 client certificate and extracts user information.
	Provides an easy interface for client certificate information.

	Copyright (c) 2016 thomas.zink_at_uni-konstanz_dot_de (tz)	
	
	Usage of the works is permitted provided that this instrument is retained with the works, so that any entity that uses the works is notified of this instrument.
	DISCLAIMER: THE WORKS ARE WITHOUT WARRANTY. 
*/

class ClientCert
{
  private $verified = false;
  private $serial = '';
  private $cn = '';
  private $o = '';
  private $ou = '';
  private $localpart = '';
  private $email = '';
  private $domain = '';
  private $validitystart = '';
  private $validityend = '';
  private $validityremain = '';
  private $subjectdn = '';
  private $issuerdn = '';
  private $issueremail = '';
  private $issuerdomain = '';
  private $issuero = '';


  public function __construct()
  {
    // certificate verified?
    $this->verified = ($_SERVER['SSL_CLIENT_VERIFY'] == 'SUCCESS');
    
    $clientcert = openssl_x509_parse($_SERVER['SSL_CLIENT_CERT']);
    $subjectaltname = $clientcert['extensions']['subjectAltName'];
    $email = substr($subjectaltname,strpos($subjectaltname,":")+1);

    $this->serial = $_SERVER['SSL_CLIENT_M_SERIAL'];
    $this->version = $_SERVER['SSL_CLIENT_M_VERSION'];
    $this->cn = $_SERVER['SSL_CLIENT_S_DN_CN'];
    $this->o = $_SERVER['SSL_CLIENT_S_DN_O'];
    $this->ou = $_SERVER['SSL_CLIENT_S_DN_OU'];
    $this->email = $email;
    list($this->localpart, $this->domain) = explode('@', $this->email);
    $this->validitystart = $_SERVER['SSL_CLIENT_V_START'];
    $this->validityend = $_SERVER['SSL_CLIENT_V_END'];
    $this->validityremain = $_SERVER['SSL_CLIENT_V_REMAIN'];
    $this->subjectdn = $_SERVER['SSL_CLIENT_S_DN'];
    $this->issuerdn = $_SERVER['SSL_CLIENT_I_DN'];
    $this->issueremail = $_SERVER['SSL_CLIENT_I_DN_Email'];
    $this->issuerdomain = substr($this->issueremail,strpos($this->issueremail,'@')+1);
    $this->issuero = $_SERVER['SSL_CLIENT_I_DN_O'];
  }

  final public function getVerified()
  {
    return $this->verified;
  }

  final public function getSerial()
  {
    return $this->serial;
  }

  final public function getVersion()
  {
    return $this->version;
  }

  final public function getLocalpart()
  {
    return $this->localpart;
  }

  final public function getEmail()
  {
    return $this->email;
  }

  final public function getDomain()
  {
    return $this->domain;
  }

  final public function getCname()
  {
    return $this->cn;
  }

  final public function getOrganization()
  {
    return $this->o;
  }

  final public function getOrganizationalUnit()
  {
    return $this->ou;
  }

  final public function getValidityStart()
  {
    return $this->validitystart;
  }

  final public function getValidityEnd()
  {
    return $this->validityend;
  }

  final public function getValidityRemain()
  {
    return $this->validityremain;
  }

  final public function getSubjectDN()
  {
    return $this->subjectdn;
  }

  final public function getIssuerDN()
  {
    return $this->issuerdn;
  }

  final public function getIssuerEmail()
  {
    return $this->issueremail;
  }

  final public function getIssuerDomain()
  {
    return $this->issuerdomain;
  }

  final public function getIssuerOrganization()
  {
    return $this->issuero;
  }
}

?>
