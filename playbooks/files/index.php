<!--
	Tests the functionality and interface of the class UserCert.

	Author: thomas.zink_at_uni-konstanz_dot_de (tz)
-->
<!DOCTYPE html>
<html>

<head>
	<title>Cert Auth</title>
</head>

<body>

<?php
require_once('clientcert.php');
$cert = new ClientCert();
?>

<?php if ($cert->getVerified()) : ?>

	<h2>Client Certificate Information</h2>
	<table>
	<tbody>

<?php
	$serial = $cert->getSerial();
	$version = $cert->getVersion();
	$cn = $cert->getCname();
	$o = $cert->getOrganization();
	$ou = $cert->getOrganizationalUnit();
	$email = $cert->getEmail();
	$user = $cert->getLocalpart();
	$domain = $cert->getDomain();
	$validfrom = $cert->getValidityStart();
	$validto = $cert->getValidityEnd();
	$validremain = $cert->getValidityRemain();
	$subjectdn = $cert->getSubjectDN();
	$issuerdn = $cert->getIssuerDN();
	$issueremail = $cert->getIssuerEmail();
	$issuerdomain = $cert->getIssuerDomain();
	$issuero = $cert->getIssuerOrganization();
	$equaldomains = strcasecmp($domain,$issuerdomain) === 0;

	echo "<tr> <td>Serial </td> <td>$serial</td> </tr>";
	echo "<tr> <td>Version </td> <td>$version</td> </tr>";
	echo "<tr> <td>CName </td> <td>$cn</td> </tr>";
	echo "<tr> <td>Organisation </td> <td>$o</td> </tr>";
	echo "<tr> <td>OrganisationalUnit </td> <td>$ou</td> </tr>";
	echo "<tr> <td>User Email </td> <td>$email</td> </tr>";
	echo "<tr> <td>Local-Part </td> <td>$user</td> </tr>";
	echo "<tr> <td>Domain </td> <td>$domain</td> </tr>";
	echo "<tr> <td>Valid From </td> <td>$validfrom</td> </tr>";
	echo "<tr> <td>Valid To </td> <td>$validto</td> </tr>";
	echo "<tr> <td>Valid Remain </td> <td>$validremain</td> </tr>";
	echo "<tr> <td>Subject DN </td> <td>$subjectdn</td> </tr>";
	echo "<tr> <td>Issuer DN </td> <td>$issuerdn</td> </tr>";
	echo "<tr> <td>Issuer Email </td> <td>$issueremail</td> </tr>";
	echo "<tr> <td>Issuer Domain </td> <td>$issuerdomain</td> </tr>";
	echo "<tr> <td>Issuer Organization </td> <td>$issuero</td> </tr>";
	echo "<tr> <td>User Domain equal to Issuer Domain? </td> <td>$equaldomains</td> </tr>";
?>
	</tbody>
	</table>

<div><hr/></div>
<div>  
  <form>
    <div>
      <label><b>Username: </b></label>
      <input type='text' name='user' value='<?php echo $cert->getEmail(); ?>' readonly /> <br/>

      <label><b>Password: </b></label>
      <input type="password" placeholder="Enter Password" name="psw" required />
    </div>

    <div style="background-color:#f1f1f1">        
      <button type="submit">Login</button>
      <input type="checkbox" checked="checked" /> Remember me
      <button type="button">Cancel</button>
      <span>Forgot <a href="#">password?</a></span>
    </div>
  </form>
</div>

<?php else: ?>
	<h2>Client certificate authentication failed</h2>
<?php endif; ?>
	
  </body>
</html>

